# Desafio Tecnico  back end 
* O desafio consiste basicamente em criar uma API para fazer a gestão de assembleias para tomada de decisões levando em consideração o voto dos associados. 

* The JVM level was changed from '11' to '17', review the [JDK Version Range](https://github.com/spring-projects/spring-framework/wiki/Spring-Framework-Versions#jdk-version-range) on the wiki for more details.

### Solução
Levando em consideração as especificações do desafio, para solução, optei em criar a API utilizando o Spring Web Flux, e um conjunto de ferramentas complementares para entrega de uma solução assícrona e reativa.

### o que foi utilizado:
* [Java 17](https://www.oracle.com/br/java/technologies/downloads/#java17)
* [Spring Reactive Web](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#web.reactive)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#using.devtools)
* [Spring Data R2DBC](https://docs.spring.io/spring-boot/docs/3.0.2/reference/htmlsingle/#data.sql.r2dbc)
* [PostgreSQL](https://www.postgresql.org/download/)
* [docker](https://www.docker.com/)

### Passos para rodar:
* clonar esse repositório
* rodar o docker compose "docker-compose up" (para container POSTGRES)
* 
### informações adicionais
xpto


